import React from 'react';
import {render} from 'react-dom';
import {Reservation} from './reservationPage/Reservation.js'
import {Call} from './callspage/Call.js'
import {List} from './listPage/List.js'
import { BrowserRouter as Router, Route } from 'react-router-dom'

//TODO: start coding

export class Root extends React.Component{

    constructor(){
        super()
    }

    render(){
        return(
            <div>
                <Router>
                    <div>
                        <Route path="/call" component={Call}/>
                        <Route exact path="/" component={List}/>
                        <Route path="/reservation" component={Reservation}/>
                    </div>
                </Router>
            </div>
        )
    }
}

render(<Root/>,document.getElementById('Root'))
